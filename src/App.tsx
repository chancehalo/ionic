import React, { useEffect } from "react";
import { Redirect, Route } from "react-router-dom";
import { IonApp, IonRouterOutlet } from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";
import { useDispatch } from "react-redux";
import socketIO from "socket.io-client";

import Create from "./pages/Create";
import Share from "./pages/Share";

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";

/* Theme variables */
import "./theme/variables.css";
import {
  setConnectedSocket,
  setShowReceivedMessageAlert
} from "./store/actions";
import { IPhoto } from "./types";
import { setReceivedPhoto } from "./store/actions";

const App: React.FC = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const socket = socketIO("http://localhost:8000");
    socket.on("photo message", (photo: IPhoto) => {
      console.log("photo message has been received");
      console.log(photo);
      dispatch(setReceivedPhoto(photo));
      dispatch(setShowReceivedMessageAlert(true));
    });
    dispatch(setConnectedSocket(socket));
  });
  return (
    <IonApp>
      <IonReactRouter>
        <IonRouterOutlet>
          <Route path="/create" component={Create} exact={true} />
          <Route exact path="/" render={() => <Redirect to="/create" />} />
          <Route path="/share" component={Share} exact={true} />
        </IonRouterOutlet>
      </IonReactRouter>
    </IonApp>
  );
};

export default App;
