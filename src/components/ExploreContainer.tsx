import React from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import "./ExploreContainer.css";
import {
  IonSlides,
  IonSlide,
  IonCard,
  IonCardSubtitle,
  IonCardContent
} from "@ionic/react";
import { setSelectedPhoto } from "../store/actions";

interface ContainerProps {}

const ExploreContainer: React.FC<ContainerProps> = () => {
  const img1 =
    "https://www.universetoday.com/wp-content/uploads/2018/08/cropped-delta_iv_heavy_streak_composite_edits.jpg";
  const img2 =
    "https://tul.imgix.net/content/general/space-docos.jpg?auto=format,compress&w=1200&h=630&fit=crop";
  const slides = [{ imgSrc: img1 }, { imgSrc: img2 }];
  return (
    <div className="container">
      <IonSlides>
        {slides.map(slide => {
          return <ImageSlide key={slide.imgSrc} imgSrc={slide.imgSrc} />;
        })}
      </IonSlides>
    </div>
  );
};

interface IImageSlideProps {
  imgSrc: string;
}

function ImageSlide(props: IImageSlideProps) {
  const dispatch = useDispatch();
  const history = useHistory();
  return (
    <IonSlide>
      <IonCard style={{ border: "5px solid blue" }}>
        <IonCardContent>
          <img
            src={props.imgSrc}
            alt="a space"
            onClick={() => {
              dispatch(setSelectedPhoto({ imgSrc: props.imgSrc }));
              history.push("/share");
            }}
          />
        </IonCardContent>
        <IonCardSubtitle>An Image</IonCardSubtitle>
      </IonCard>
    </IonSlide>
  );
}

export default ExploreContainer;
