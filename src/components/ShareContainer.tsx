import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { AppState } from "../store";
import "./ShareContainer.css";
import { IonCard, IonList, IonAlert } from "@ionic/react";
import { IPhoto } from "../types";
import { setShowReceivedMessageAlert } from "../store/actions";

interface ContainerProps {}

const ShareContainer: React.FC<ContainerProps> = props => {
  const receivedPhoto: IPhoto = useSelector(
    (state: AppState) => state.photos.receivedPhoto
  );
  const [showImage, setShowImage] = useState(false);
  const dispatch = useDispatch();
  const socket: SocketIOClient.Socket = useSelector(
    (state: AppState) => state.socket.socket
  );
  const selectedPhoto: IPhoto = useSelector(
    (state: AppState) => state.photos.selectedPhoto
  );
  const showAlert: boolean = useSelector(
    (state: AppState) => state.socket.showReceivedMessageAlert
  );
  return (
    <div className="container">
      <IonAlert
        isOpen={showAlert}
        onDidDismiss={() => dispatch(setShowReceivedMessageAlert(false))}
        header={"New Message!"}
        buttons={[
          {
            text: "Dismiss",
            role: "cancel"
          },
          {
            text: "View",
            handler: () => {
              setShowImage(true);
            }
          }
        ]}
      />
      <IonList>
        {showImage && (
          <IonCard>
            <img src={receivedPhoto.imgSrc} />
          </IonCard>
        )}
        <IonCard
          onClick={() => {
            socket.emit("photo message", selectedPhoto);
          }}
        >
          Send Message
        </IonCard>
      </IonList>
    </div>
  );
};

export default ShareContainer;
