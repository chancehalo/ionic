import React from "react";
import {
  IonPage,
  IonHeader,
  IonToolbar,
  IonTitle,
  IonContent
} from "@ionic/react";
import ShareContainer from "../components/ShareContainer";

export default function Share() {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Share</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Share</IonTitle>
          </IonToolbar>
        </IonHeader>
        <ShareContainer />
      </IonContent>
    </IonPage>
  );
}
