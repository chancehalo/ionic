import {
  SET_SOCKET,
  SET_SELECTED_PHOTO,
  SET_SHOW_RECEIVED_MESSAGE_ALERT
} from "./actionTypes";
import { IPhoto } from "../types";
import { SET_RECEIVED_PHOTO } from "./actionTypes";

export const setConnectedSocket = (socket: SocketIOClient.Socket) => ({
  type: SET_SOCKET,
  payload: socket
});

export const setSelectedPhoto = (photo: IPhoto) => ({
  type: SET_SELECTED_PHOTO,
  payload: photo
});

export const setReceivedPhoto = (photo: IPhoto) => ({
  type: SET_RECEIVED_PHOTO,
  payload: photo
});

export const setShowReceivedMessageAlert = (flag: boolean) => ({
  type: SET_SHOW_RECEIVED_MESSAGE_ALERT,
  payload: flag
});
