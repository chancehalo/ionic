import { combineReducers } from "redux";
import photosReducer from "./reducers/photoReducer";
import socketReducer from "./reducers/socketReducer";

const rootReducer = combineReducers({
  photos: photosReducer,
  socket: socketReducer
});
export type AppState = ReturnType<typeof rootReducer>;
export default rootReducer;
