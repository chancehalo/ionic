import { SET_SELECTED_PHOTO, SET_RECEIVED_PHOTO } from "../actionTypes";
import { IPhoto } from "../../types";

type PhotosState = {
  selectedPhoto: IPhoto;
  receivedPhoto: IPhoto;
};
const initialPhotoState: PhotosState = {
  selectedPhoto: { imgSrc: "" },
  receivedPhoto: { imgSrc: "" }
};

const photosReducer = (state = initialPhotoState, action: any) => {
  switch (action.type) {
    case SET_SELECTED_PHOTO:
      return {
        ...state,
        selectedPhoto: action.payload
      };
    case SET_RECEIVED_PHOTO:
      return {
        ...state,
        receivedPhoto: action.payload
      };
    default:
      return state;
  }
};

export default photosReducer;
