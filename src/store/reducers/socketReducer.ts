import { SET_SOCKET, SET_SHOW_RECEIVED_MESSAGE_ALERT } from "../actionTypes";

type SocketState = {
  socket: SocketIOClient.Socket | undefined;
  showReceivedMessageAlert: boolean;
};
const initialSocketState: SocketState = {
  socket: undefined,
  showReceivedMessageAlert: false
};

const socketReducer = (state = initialSocketState, action: any) => {
  switch (action.type) {
    case SET_SOCKET:
      return {
        ...state,
        socket: action.payload
      };
    case SET_SHOW_RECEIVED_MESSAGE_ALERT:
      return {
        ...state,
        showReceivedMessageAlert: action.payload
      };
    default:
      return state;
  }
};

export default socketReducer;
